// tslint:disable-next-line:variable-name
const noticias_state_model = require("~/app/domain/noticias-state.models")

// tslint:disable-next-line:only-arrow-functions
describe("reducersNoticias", function () {
    // tslint:disable-next-line:only-arrow-functions
    it("should reduce init  data", function (){
        const prevState = noticias_state_model.initializeNoticiasState();
        const action = new noticias_state_model.InitMyDataAction(["noticia1", "noticia2"]);
        const newState = noticias_state_model.reducersNoticias(prevState, action)
        expect(newState.items.length).toEqual(2);
        // expect(newState.items[0]).toEqual("noticia1");
    })

    // tslint:disable-next-line:only-arrow-functions
    it("should reduce new item added", function(){
        const prevState = noticias_state_model.initializeNoticiasState();
        const action = new noticias_state_model.NuevaNoticiaAction(new noticias_state_model.Noticia("Esta Es la noticia!"));
        const newState = noticias_state_model.reducersNoticias(prevState, action)
        expect(newState.items.length).toEqual(1);
        // expect(newState.items[0]).toEqual("Esta Es la noticia!");
    })
})
