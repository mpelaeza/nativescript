import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ContactsService } from "../services/contacts.service";
import * as Toast from "nativescript-toast";
import { Store } from "@ngrx/store";
import { AppState } from "~/app/app.module.ts";
import { NuevaLlamadaAction } from "~/app/domain/llamadas-state.models.ts";

@Component({
    selector: "Featured",
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {
    favoritos: Array<string>;

    constructor(private contacts: ContactsService, private store: Store<AppState>) {
        this.favoritos = this.contacts.favs();
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onTap(contact: string) {
        this.store.dispatch(new NuevaLlamadaAction(contact))
        const toast = Toast.makeText(`Llamada a ${contact}`, "short");
        toast.show();
    }
}
