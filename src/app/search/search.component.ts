import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "~/app/services/noticias.service.ts";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {

    constructor(private noticias: NoticiasService) {
        if (this.noticias.size() === 0) {
            this.noticias.agregar("Hola!");
            this.noticias.agregar("Hola1!");
            this.noticias.agregar("Hola2!");
        }
    }

    ngOnInit(): void {
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
