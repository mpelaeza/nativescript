import { Action } from "@ngrx/store";

// ESTADOs

export interface LlamadasState {
    items: Array<string>;
}

export function initializeLlamadasState() {
    return {
        items: [],
        sugerida: null
    };
}

export enum LlamadasActionTypes {
    NUEVA_LLAMADA = "[Llamada] Nueva"
}

// tslint:disable-next-line:max-classes-per-file
export class NuevaLlamadaAction implements Action {
    type = LlamadasActionTypes.NUEVA_LLAMADA;
    constructor(public contacto: string) {}
}

export type LlamadasActions = NuevaLlamadaAction;

// Reducers
export function reducersLlamadas(
    state: LlamadasState,
    action: LlamadasActions
): LlamadasState {
    switch (action.type) {
        case LlamadasActionTypes.NUEVA_LLAMADA: {

            return {
                ...state,
                items: [...state.items, (action as NuevaLlamadaAction).contacto]
            };
        }
    }

    return state;
}
