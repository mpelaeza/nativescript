import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Observable } from "rxjs";
import { Action } from "@ngrx/store";
import { map } from "rxjs/internal/operators";

// ESTADO
export class Noticia {
    constructor(titulo: string) {
        // Empty line }
    }
}

export interface NoticiasState {
    items: Array<Noticia>;
    sugerida: Noticia;
}

export function initializeNoticiasState() {
    return {
        items: [],
        sugerida: null
    };
}

export enum NoticiasActionTypes {
    INIT_MY_DATA = "[Noticias] Init My Data",
    NUEVA_NOTICIA = "[Noticias] Nueva",
    SUGERIR_NOTICIA = "[Noticias] sugerir"
}

// tslint:disable-next-line:max-classes-per-file
export class InitMyDataAction  implements Action {
    type = NoticiasActionTypes.INIT_MY_DATA;
    constructor(public titulares: Array<Noticia>) {}
}

// tslint:disable-next-line:max-classes-per-file
export class NuevaNoticiaAction implements Action {
    type = NoticiasActionTypes.NUEVA_NOTICIA;
    constructor(public noticia: Noticia) {}
}

// tslint:disable-next-line:max-classes-per-file
export class SugerirNoticiaAction implements Action {
    type = NoticiasActionTypes.SUGERIR_NOTICIA;
    constructor(public noticia: Noticia) {}
}

export type NoticiasActions = NuevaNoticiaAction | InitMyDataAction;

// Reducers
export function reducersNoticias(
    state: NoticiasState,
    action: NoticiasActions
): NoticiasState {
    switch (action.type) {
        case NoticiasActionTypes.INIT_MY_DATA: {

            // @ts-ignore
            const titulares: Array<string> = (action as InitMyDataAction).titulares;

            return {
                ...state,
                items: titulares.map((t) => new Noticia(t))
            };
        }
        case NoticiasActionTypes.NUEVA_NOTICIA: {
            return {
                ...state,
                items: [...state.items, (action as NuevaNoticiaAction).noticia]
            };
        }
        case NoticiasActionTypes.SUGERIR_NOTICIA: {
            return {
                ...state,
                sugerida: (action as SugerirNoticiaAction).noticia
            };
        }
    }

    return state;
}

// Effects
// tslint:disable-next-line:max-classes-per-file
@Injectable()
export class NoticiasEffects {
    @Effect()
    nuevosAgregados$: Observable<Action> = this.actions$.pipe(
        ofType(NoticiasActionTypes.NUEVA_NOTICIA),
        map((action: NuevaNoticiaAction) => new SugerirNoticiaAction((action.noticia)))
    );
    constructor(private actions$: Actions) {
    }
}
