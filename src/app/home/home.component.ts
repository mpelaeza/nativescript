import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Store } from "@ngrx/store";
import { AppState } from "~/app/app.module.ts";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    os: string = "hola";
    llamadas: Array<string>;
    constructor(private store: Store<AppState>) {
        this.store.select((state) => state.llamadas.items)
            .subscribe((llamadas) => this.llamadas = llamadas);
    }

    ngOnInit(): void {
        // test
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
