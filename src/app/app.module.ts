import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NoticiasService } from "~/app/services/noticias.service.ts";

import {
    initializeNoticiasState,
    NoticiasEffects,
    NoticiasState,
    reducersNoticias
} from "./domain/noticias-state.models";
import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { initializeLlamadasState, LlamadasState, reducersLlamadas } from "~/app/domain/llamadas-state.models.ts";

export interface AppState {
    noticias: NoticiasState;
    llamadas: LlamadasState;
}

const reducers: ActionReducerMap<AppState> = {
    noticias: reducersNoticias,
    llamadas: reducersLlamadas
};

const reducersInitialState = {
    noticias: initializeNoticiasState(),
    llamadas: initializeLlamadasState()
};

@NgModule({
    bootstrap: [
        AppComponent
    ],
    providers: [NoticiasService],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NativeScriptUIListViewModule,
        NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}),
        EffectsModule.forRoot([NoticiasEffects])
    ],
    declarations: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
