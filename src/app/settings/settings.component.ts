import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    private nombreUsuario: string;
    private emailUsuario: string;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.nombreUsuario = appSettings.getString("nombreUsuario", "Anónimo");
        this.emailUsuario =  appSettings.getString("emailUsuario", "anonimo@ejemplo.com");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

}
