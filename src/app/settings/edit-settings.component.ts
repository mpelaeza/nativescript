import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";
import * as camera from "nativescript-camera";
import { fromFile, ImageSource } from "tns-core-modules/image-source";
import * as SocialShare from "nativescript-social-share";


import { Router } from "@angular/router";

@Component({
    selector: "EditSettings",
    templateUrl: "./edit-settings.component.html"
})
export class EditSettingsComponent implements OnInit {
    nombreUsuario: string;
    emailUsuario: string;
    imagePath: string;

    constructor(private router: Router) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.nombreUsuario =  appSettings.getString("nombreUsuario", "Anónimo");
        this.emailUsuario =  appSettings.getString("emailUsuario", "anonimo@ejemplo.com");
        this.imagePath =  appSettings.getString("imagePath", "res://foto_perfil");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    editUser() {
        appSettings.setString("nombreUsuario", this.nombreUsuario);
        appSettings.setString("emailUsuario", this.emailUsuario);
        // appSettings.setString("imagePath", this.imagePath);
        this.router.navigate([""]);
    }

    onImageTap(): void {
        camera.requestPermissions().then(
            function success() {
                const options = {width: 300, height: 300, keepAspectRatio: false, saveToGallery: false};
                console.log(camera.isAvailable());
                camera.takePicture(options)
                    .then((imageAsset) => {
                        ImageSource.fromAsset(imageAsset)
                            .then((imageSource) => {
                                SocialShare.shareImage(imageSource, "Imagen de prueba");
                                this.imagePath = imageSource;
                            });
                    }).catch((err) => {
                    console.log("Error -> " + err.message);
                });
            },
            function failure() {
                console.log("error!!!");
            }
        );
    }
}
