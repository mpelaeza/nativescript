import { Injectable } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http";

@Injectable()
export class NoticiasService {
    api: string = "https://83de26efaf79.ngrok.io";
    private noticias: Array<string> = [];

    agregar(s: string) {
        this.noticias.push(s);

        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-type": "application/json"},
            content: JSON.stringify({nuevo: s})
        });

    }

    favs() {
        return getJSON(this.api + "/favs");
    }

    buscar(s: string) {
        console.log(this.api + "/get?q=" + s);

        return getJSON(this.api + "/get?q=" + s);
    }

    size(): number {
        return this.noticias.length;
    }
}
