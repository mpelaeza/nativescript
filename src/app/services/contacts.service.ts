import { Injectable } from "@angular/core";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class ContactsService {
    private contacts: Array<object> = [];

    constructor() {
        console.log("iniciando DB");
        this.getDb(
            (db) => console.dir(db),
            () => console.log("error en getDB")
            );
    }

    agregar(contact: object) {
        this.contacts.push(contact);
    }

    agregarInicio(contact: object) {
        this.contacts.unshift(contact);
    }

    listar() {
        return this.contacts;
    }

    buscar(index: number) {
        return this.contacts[index];
    }

    size(): number {
        return this.contacts.length;
    }

    editNumber(index: number, phone: string) {
        const contact = this.buscar(index);
        console.dir(contact);
        // @ts-ignore
        contact.phone = phone;
        this.contacts[index] = contact;
    }

    edit(index: number, contactInfo: any) {
        // const contact = this.buscar(index);
        this.contacts[index] = contactInfo;
    }

    delete(index: number) {
        return this.contacts.splice(index, 1);
    }

    favs(): Array<any> {
        let favs = [];
        this.getDbConnection().then((db) => {
            db.all("SELECT nombre FROM favs").then((results) => {
                results.forEach((nombre) => {
                    favs.push(nombre[0]);
                });
            });
        });

        return favs;
    }

    addFav(index: number) {
        const contact = this.buscar(index);
        const name = contact['name']
        if (contact['fav'] === true) {
            return 0;
        }
        contact['fav'] = true;
        this.edit(index, {...contact});
        let savedId = 0;
        console.log(name);
        this.getDbConnection().then((db) => {
            db.execSQL(`INSERT INTO favs (nombre) VALUES ("${name}")`).then((id) => console.log(id));
        });

        return savedId;
    }

    getDbConnection() {
        return new sqlite("mi_db");
    }

    closedDbConnection() {
        new sqlite("mi_db")
            .then((db) => {
                db.close();
            });
    }

    private getDb(fnOk: (db: any) => void, fnError: () => void) {
        return new sqlite("mi_db", (err, db) => {
            if (err) {
                console.error("error al iniciar db", err);
            } else {
                db.execSQL("CREATE TABLE IF NOT EXISTS favs (id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT UNIQ)")
                    .then((id) => {
                        console.log("CREATE TABLE OK");
                        fnOk(db);
                    }, (error) => {
                        console.error(error);
                        fnError();
                    });
            }
        });
    }


}
