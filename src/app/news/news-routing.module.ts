import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { NewsComponent } from "./news.component";
import { FriendsNewsComponent } from "~/app/news/friends-news/friends-news.component.ts";

const routes: Routes = [
    { path: "", component: NewsComponent },
    { path: "friends", component: FriendsNewsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class NewsRoutingModule { }
