import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { NoticiasService } from "~/app/services/noticias.service.ts";
import * as Toast from "nativescript-toast";

@Component({
    selector: "News",
    templateUrl: "./news.component.html"
})
export class NewsComponent implements OnInit {
    resultados: Array<string>;
    textFieldValue: string = "";
    constructor(private router: Router, private routerExtensions: RouterExtensions, private noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.buscarAhora('');
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

    onItemTap(event): void {
        console.dir(event);
    }

    buscarAhora(s: string) {
        console.dir("buscar ahora:" + s);
        this.noticias.buscar(s).then((r: any) => {
            this.resultados = r;
        }, (e) => {
            console.log("Error al buscar ahora");
            this.resultados = [];
            const toast = Toast.makeText("Error en la busqueda", "long");
            toast.show();
            }
        );

        return this.resultados;
    }

    buscar() {
        this.buscarAhora(this.textFieldValue);
    }
}
