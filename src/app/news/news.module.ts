import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NewsRoutingModule } from "./news-routing.module";
import { NewsComponent } from "./news.component";
import { FriendsNewsComponent } from "~/app/news/friends-news/friends-news.component.ts";
import { NativeScriptFormsModule } from "@nativescript/angular";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NewsRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        NewsComponent,
        FriendsNewsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NewsModule { }
