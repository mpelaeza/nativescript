import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ContactListComponent } from "./contact-list.component";
import { ContactDetailComponent } from "~/app/contact-list/contact-detail/contact-detail.component";
import { EditContactDetailComponent } from "~/app/contact-list/edit-contact-detail/edit-contact-detail.component.ts";

const routes: Routes = [
    { path: "", component: ContactListComponent },
    { path: ":id", component: ContactDetailComponent },
    { path: ":id/edit", component: EditContactDetailComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ContactListRoutingModule { }
