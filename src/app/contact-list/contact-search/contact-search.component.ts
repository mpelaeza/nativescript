import { Component, ElementRef, EventEmitter, Output, ViewChild } from "@angular/core";
import { View } from "tns-core-modules/ui/core/view/view";
import { setInterval } from "tns-core-modules/timer";

@Component({
    selector: "ContactSearch",
    moduleId: module.id,
    template: `
        <FlexboxLayout flexDirection="row">
            <TextField #texto="ngModel" [(ngModel)]="textFieldValue" width="80%" required></TextField>
             <Label *ngIf="texto.hasError('required')" text="*"></Label>
             <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="*"></Label>
            <FlexboxLayout #layout2 style="padding-right: 10px">
                <Button class="-primary" text="Buscar" (tap)="onButtonTab()" *ngIf="texto.valid && !isBusy" ></Button>
                <ActivityIndicator #activityIndicator [busy]="isBusy" *ngIf="isBusy">
                </ActivityIndicator>
            </FlexboxLayout>
        </FlexboxLayout>
    `
})
export class ContactSearchComponent {
    textFieldValue: string = "";
    isBusy: boolean = false;
    @ViewChild("layout2", { static: true }) layout2: ElementRef;
    @ViewChild("activityIndicator", { static: true }) indicator: ElementRef;
    @Output() search: EventEmitter<string> = new EventEmitter<string>();
    onButtonTab(): void {
        if (this.textFieldValue.length > 2) {
            const buscar = <View>this.layout2.nativeElement;
            buscar.animate({
                rotate: -360,
                duration: 300,
                delay: 150
            }).then(() => buscar.animate({
                rotate: 360,
                duration: 300,
                delay: 150
            })).then(() => this.isBusy = true);

            this.search.emit(this.textFieldValue);
            setInterval(() => {
                this.isBusy = false;
            }, 2000);
        }
    }
}
