import { Directive, Input } from"@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from"@angular/forms";
@Directive(
    { selector: "[minlen]",
      providers: [{provide: NG_VALIDATORS, useExisting: MinLenDirective, multi: true}]
    })

export class MinLenDirective implements Validator {
    @Input() minlen: string;

    constructor() {
        // test
    }

    validate(control: AbstractControl): {[key: string]: any} {
        return !control.value || control.value.length >= (this.minlen || 3) ? null : { minlen: true };
    }
}
