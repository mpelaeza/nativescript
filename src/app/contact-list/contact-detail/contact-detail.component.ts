import { Component, OnInit } from "@angular/core";
import * as app from "tns-core-modules/application";
import { ActivatedRoute, Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { ContactsService } from "../../services/contacts.service";

@Component({
    selector: "ContactDetail",
    templateUrl: "./contact-detail.component.html"
})
export class ContactDetailComponent implements OnInit {
    contact: object;
    constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions,
                private contacts: ContactsService) {
        this.route.params.subscribe((params) => this.contact = contacts.buscar(params.id));
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    backButtonTab(): void {
        this.routerExtensions.navigate(["/contacts"]);
    }
}
