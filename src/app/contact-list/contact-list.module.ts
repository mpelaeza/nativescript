import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { ContactListRoutingModule } from "./contact-list-routing.module";
import { ContactListComponent } from "./contact-list.component";
import { ContactsService } from "~/app/services/contacts.service";
import { ContactDetailComponent } from "~/app/contact-list/contact-detail/contact-detail.component";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { ContactSearchComponent } from "~/app/contact-list/contact-search/contact-search.component.ts";
import { MinLenDirective } from "~/app/contact-list/contact-search/min-len.directive.ts";
import { EditContactDetailComponent } from "~/app/contact-list/edit-contact-detail/edit-contact-detail.component.ts";

@NgModule({
    providers: [ContactsService],
    imports: [
        NativeScriptCommonModule,
        ContactListRoutingModule,
        NativeScriptUIListViewModule,
        NativeScriptFormsModule
    ],
    declarations: [
        ContactListComponent,
        ContactDetailComponent,
        EditContactDetailComponent,
        ContactSearchComponent,
        MinLenDirective
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ContactListModule { }
