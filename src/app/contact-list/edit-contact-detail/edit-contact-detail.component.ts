import { Component, OnInit } from "@angular/core";
import * as app from "tns-core-modules/application";
import { ActivatedRoute, Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { ContactsService } from "../../services/contacts.service";
import * as Toast from "nativescript-toast";

@Component({
    selector: "ContactDetail",
    templateUrl: "./edit-contact-detail.component.html"
})
export class EditContactDetailComponent implements OnInit {
    index: number;
    contact: object;
    nameFieldValue: string;
    phoneFieldValue: string;

    constructor(private route: ActivatedRoute, private routerExtensions: RouterExtensions,
                private contacts: ContactsService) {
        this.route.params.subscribe((params) => {
            this.contact = contacts.buscar(params.id);
            this.index = params.id;
        });
        this.nameFieldValue = this.contact['name'];
        this.phoneFieldValue = this.contact['phone'];
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    backButtonTab(): void {
        this.routerExtensions.navigate(["/contacts"]);
    }

    onButtonTab() {
        this.contacts.edit(this.index, {name: this.nameFieldValue, phone: this.phoneFieldValue, urlImage: this.contact['urlImage']});
        const toast = Toast.makeText("Se editó correctamente", "long");
        toast.show();
        this.routerExtensions.navigate(["/contacts"]);
    }
}
