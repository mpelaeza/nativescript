import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { ContactsService } from "~/app/services/contacts.service.ts";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toast";
import * as SocialShare from "nativescript-social-share";
import { Color, View } from "tns-core-modules/ui/core/view/view";
import { fromFile, ImageSource } from "tns-core-modules/image-source";

@Component({
    selector: "ContactList",
    templateUrl: "./contact-list.component.html"
})

export class ContactListComponent implements OnInit {
    contactList: Array<any>;
    favorites: Array<any>;
    @ViewChild("layout", { static: false}) layout: ElementRef;
    constructor(private router: Router, private routerExtensions: RouterExtensions, private contacts: ContactsService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        if (this.contacts.size() === 0) {
            this.contacts.agregar({name: "Maria", phone: "2222222", urlImage: "https://api.adorable.io/avatars/285/Maria.png"});
            this.contacts.agregar({name: "Juan", phone: "3333333", urlImage: "https://api.adorable.io/avatars/285/Juan.png"});
            this.contacts.agregar({name: "Pedro", phone: "444444444", urlImage: "https://api.adorable.io/avatars/285/Pedro.png"});
            this.contacts.agregar({name: "Mario", phone: "5823322", urlImage: "https://api.adorable.io/avatars/285/Mario.png"});
        }
        this.contactList = this.contacts.listar();
        this.favorites = this.contacts.favs();
        console.log(this.favorites);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

    onItemTap(event): void {
        console.log(event.index);
        this.routerExtensions.navigate(["contacts", event.index], {
            transition: {
                name: "fade"
            }
     });
    }

    onPullToRefreshInitiated(event) {
        setTimeout(() => {
            console.dir(event);
            this.contacts.agregarInicio({name: "Nuevo Contacto", phone: "1234567", urlImage: `https://api.adorable.io/avatars/285/${Math.random().toString(36).substring(7)}`});
            const listView = event.object;
            listView.notifyPullToRefreshFinished();
            }, 1000);
    }

    onLoadMoreDataInitiated(event) {
        setTimeout(() => {
            console.dir(event);
            this.contacts.agregar({name: "Nuevo Contacto", phone: "1234567", urlImage: `https://api.adorable.io/avatars/285/${Math.random().toString(36).substring(7)}`});
            const listView = event.object;
            listView.notifyLoadOnDemandFinished();
            }, 1000);
    }

    editContactPhone(index: number) {
        dialogs.action("Cambiar Numero", "Cancelar!", ["123456", "121212"])
            .then((result) => {
                if (result === "123456") {
                    this.contacts.editNumber(index, "123456");
                } else {
                    this.contacts.editNumber(index, "121212");
                }
                const toast = Toast.makeText("Se edito correctamente", "long");
                toast.show();
            });
    }

    deleteContact(index: number) {
        this.contacts.delete(index);
        dialogs.alert({message: "Se eliminó correctamente el contacto", okButtonText: "Aceptar"});
    }

    buscar(s: string) {
        this.contactList  = this.contacts.listar().filter((contact) => contact['name'].indexOf(s) >= 0);
        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        }));
    }

    editContact(index: number) {
        console.log(index);
        this.routerExtensions.navigate(["contacts", index, "edit"], {
            transition: {
                name: "fade"
            }
        });
    }

    onLongPress(index: number) {
        dialogs.action("Acción", "Cancelar", ["Compartir", "Compartir imagen", "Editar", "Eliminar"])
            .then((result) => {
                if (result === "Editar") {
                    this.editContact(index);
                } else if (result === "Eliminar") {
                    this.deleteContact(index);
                    const toast = Toast.makeText(`Se eliminó correctamente`, "long");
                    toast.show();
                } else if (result === "Compartir") {
                    const contact = this.contacts.buscar(index);
                    SocialShare.shareText(contact['name'], "Hola mundo");
                } else if (result === "Compartir imagen") {
                    const contact = this.contacts.buscar(index);
                    console.log("hola");
                    ImageSource.fromUrl(contact['urlImage'])
                        .then((img) => { SocialShare.shareImage(img, "Foto de perfil"); });


                }
            });
    }

    fav(index: number) {
        this.favorites.push(this.contacts.buscar(index)['name'])
        this.contacts.addFav(index);
        const toast = Toast.makeText(`Se agregó a favoritos`, "long");
        toast.show();
    }
}
